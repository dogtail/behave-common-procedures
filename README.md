# Project behave-common-procedures
This project is a kind of an interface between accessibility API library dogtail and behave. Behave uses tests written in a natural language style, backed up by Python code. 

This project expect a behave file structure, in which we will initiate our data and we use behave before/after code structure to execute code needed in most of our automation. 

This submodule is used as a library which provides many features/functions/procedures which are very useful in tests.


## Getting Started
To use this submodule use command to add this to your project:
```
git submodule add -b master https://gitlab.com/dogtail/behave-common-procedures common_procedures --force
```


### Prerequisites
To use some parts of the submodule you may need some libraries:

To use opencv image matching you will need (this will be needed in CI and you need to include the package in your mapper.yaml):
```
sudo pip3 install opencv-python
```
And to use included error message format in color you will need (this will not be needed in CI):
```
sudo pip3 install termcolor
```


### Setup in environment
You need to change your **environment.py** file. This is an example how to setup your tests:

```
import sys
import traceback
from common_procedures.sandbox import TestSandbox


def before_all(context):
    try:
        context.sandbox = TestSandbox("gnome-terminal")

        context.terminal = context.sandbox.get_app("gnome-terminal", a11yAppName="gnome-terminal-server")
    except Exception as error:
        print(f"Environment error: before_all: {error}")
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)


def before_scenario(context, scenario):
    try:
        context.sandbox.before_scenario(context, scenario)
    except Exception as error:
        print(f"Environment error: before_scenario: {error}")
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)


def after_scenario(context, scenario):
    try:
        context.sandbox.after_scenario(context, scenario)
    except Exception as error:
        print(f"Environment error: after_scenario: {error}")
        traceback.print_exc(file=sys.stdout)

```


### Setup in main code
When you run your tests now and there is no exception caught in environment. That means the environment was setup correctly. To use this setup in your **steps.py** file you need another import which will import precoded steps:
```
from common_procedures.common_steps import *
```
And if you would like image matching you will need:
```
from common_procedures.image_matching import *
```


### Usage in steps.py
```
context.terminal.instance.<dogtail_stuff>
context.firefox.instance.<dogtail_stuff>
context.nautilus.instance.<dogtail_stuff>
```
You get the idea.


### Usage in .feature files.
Examples:
```
  * Start application "terminal" via "menu"
  * Close application "terminal" via "shortcut"

  * Left click "System" "menu" in "gnome-shell"
  * Left click "Settings" "push button" that is "showing" in "gnome-shell"
  * Item "Settings" "menu" is "showing" in "gnome-shell"
```



## Files included explained
* [__init__.py](https://gitlab.cee.redhat.com/desktopqe/behave-common-procedures/blob/rhel-8/__init__.py) - collection of useful functions.
* [application.py](https://gitlab.cee.redhat.com/desktopqe/behave-common-procedures/blob/rhel-8/application.py) - application class that contains methods to deal with applications.
* [common_steps.py](https://gitlab.cee.redhat.com/desktopqe/behave-common-procedures/blob/rhel-8/common_steps.py) - behave wrapper for dogtail.
* [get_node.py](https://gitlab.cee.redhat.com/desktopqe/behave-common-procedures/blob/rhel-8/get_node.py) - get_node class that does all asserting and searching for desired node wanted via common_steps.py
* [image_matching.py](https://gitlab.cee.redhat.com/desktopqe/behave-common-procedures/blob/rhel-8/image_matching.py) - opencv image comparison.
* [sandbox.py](https://gitlab.cee.redhat.com/desktopqe/behave-common-procedures/blob/rhel-8/sandbox.py) - main testing class which takes care of all system settings for successful test run.
* [start_recording.py](https://gitlab.cee.redhat.com/desktopqe/behave-common-procedures/blob/rhel-8/start_recording.py) - script for start recording test via dbus.
* [time_limit.py](https://gitlab.cee.redhat.com/desktopqe/behave-common-procedures/blob/rhel-8/time_limit.py) - experimental function for time limiting certain tests.
